import {Entity, model, property} from '@loopback/repository';

@model()
export class Cliente extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  strNombre: string;

  @property({
    type: 'string',
    required: true,
  })
  strApellidoPaterno: string;

  @property({
    type: 'string',
    required: true,
  })
  strApellidoMaterno: string;

  @property({
    type: 'string',
    required: true,
  })
  strRFC: string;

  @property({
    type: 'string',
    required: true,
  })
  strDireccion: string;

  @property({
    type: 'string',
    required: true,
  })
  strGenero: string;

  @property({
    type: 'number',
    required: true,
  })
  intEdad: number;

  constructor(data?: Partial<Cliente>) {
    super(data);
  }
}

export interface ClienteRelations {
  // describe navigational properties here
}

export type ClienteWithRelations = Cliente & ClienteRelations;
