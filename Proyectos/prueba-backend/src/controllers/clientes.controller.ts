import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {Cliente} from '../models';
import {ClienteRepository} from '../repositories';

export class ClientesController {
  constructor(
    @repository(ClienteRepository)
    public clienteRepository: ClienteRepository,
  ) {}

  @get('/clientes')
  @response(200, {
    description: 'Array of cliente model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Cliente, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Cliente) filter?: Filter<Cliente>,
  ): Promise<Cliente[]> {
    return this.clienteRepository.find(filter);
  }

  @post('/clientes')
  @response(200, {
    description: 'cliente model instance',
    content: {'application/json': {schema: getModelSchemaRef(Cliente)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Object, {
            title: 'cliente',
          }),
        },
      },
    })
    cliente: Omit<Cliente, 'id'>,
  ): Promise<object> {
    try {
      console.log('URL api ----POST----- /clientes');
      const {strRFC} = cliente;
      const validateRFC = await this.clienteRepository.findOne({
        where: {strRFC: strRFC},
        fields: {id: true},
      });
      if (validateRFC) {
        return {success: false, message: 'El RFC ya esta registrado'};
      } else {
        await this.clienteRepository.create(cliente);
        return {success: true, message: 'Registrado con exito'};
      }
    } catch (error) {
      console.error('error: ', error);
      throw error;
    }
  }

  @get('/clientes/count')
  @response(200, {
    description: 'cliente model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Cliente) where?: Where<Cliente>): Promise<Count> {
    return this.clienteRepository.count(where);
  }

  @patch('/clientes')
  @response(200, {
    description: 'cliente PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Cliente, {partial: true}),
        },
      },
    })
    cliente: Cliente,
    @param.where(Cliente) where?: Where<Cliente>,
  ): Promise<Count> {
    return this.clienteRepository.updateAll(cliente, where);
  }

  @get('/clientes/{id}')
  @response(200, {
    description: 'cliente model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Cliente, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Cliente, {exclude: 'where'})
    filter?: FilterExcludingWhere<Cliente>,
  ): Promise<Cliente> {
    return this.clienteRepository.findById(id, filter);
  }

  @patch('/clientes/{id}')
  @response(204, {
    description: 'cliente PATCH success',
  })
  async updateById(
    @param.path.number('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Cliente, {partial: true}),
        },
      },
    })
    cliente: Cliente,
  ): Promise<void> {
    await this.clienteRepository.updateById(id, cliente);
  }

  @put('/clientes/{id}')
  @response(204, {
    description: 'cliente PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() cliente: Cliente,
  ): Promise<void> {
    await this.clienteRepository.replaceById(id, cliente);
  }

  @del('/clientes/{id}')
  @response(204, {
    description: 'cliente DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.clienteRepository.deleteById(id);
  }
}
