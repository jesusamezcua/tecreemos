import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DsMongoDbDataSource} from '../datasources';
import {Cliente, ClienteRelations} from '../models';

export class ClienteRepository extends DefaultCrudRepository<
  Cliente,
  typeof Cliente.prototype.strRFC,
  ClienteRelations
> {
  constructor(
    @inject('datasources.DSMongoDB') dataSource: DsMongoDbDataSource,
  ) {
    super(Cliente, dataSource);
  }
}
