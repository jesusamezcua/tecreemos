export class Usuario {
    email: string = '';
    password: string = '';
    returnSecureToken: boolean = true;

    constructor(jsnForm: Usuario) {
        this.email = jsnForm['email'];
        this.password = jsnForm['password'];
        this.returnSecureToken = jsnForm['returnSecureToken'];
    }
}
