export class Cuenta {
    estado: string = '';
    fechaUltimaAct: string = '';
    idCliente: string = '';
    numeroCuenta: number = 0;
    saldo: number = 0.0;

    constructor(jsnForm: any) {
        this.estado = 'Activa';
        this.idCliente = jsnForm['idCliente'];
        this.numeroCuenta = jsnForm['numeroCuenta'];
        this.fechaUltimaAct = jsnForm['fechaUltimaAct'] || new Date().toLocaleDateString('En-en'); ;
        this.saldo = jsnForm['saldo'];
    }
}

