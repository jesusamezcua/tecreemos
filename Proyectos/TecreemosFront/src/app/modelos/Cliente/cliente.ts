export class Cliente {
    id:string = '';
    strNombre:string = '';
    strApellidoPaterno:string = '';
    strApellidoMaterno:string = '';
    strRFC:string = '';
    strDireccion: string = '';
    strGenero:string = '';
    intEdad:number = 0;

    constructor(jsnForm: any) {
        this.id = jsnForm['id'] || null;
        this.strNombre = jsnForm['strNombre'];
        this.strApellidoPaterno = jsnForm['strApellidoPaterno'];
        this.strApellidoMaterno = jsnForm['strApellidoMaterno'];
        this.strRFC = jsnForm['strRFC'];
        this.strDireccion = jsnForm['strDireccion'];
        this.strGenero = jsnForm['strGenero'];
        this.intEdad = jsnForm['intEdad'];
    }
}
