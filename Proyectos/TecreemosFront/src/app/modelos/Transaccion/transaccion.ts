export class Transaccion {
    fechaUltimaAct: string = '';
    monto: number = 0.0;
    numeroCuenta: number = 0;
    terminal: string = '';
    tipo: string = '';
    usuario: string = '';

    constructor(jsnForm: any) {
        this.fechaUltimaAct = jsnForm['fechaUltimaAct'] || new Date().toLocaleString(); ;
        this.monto = jsnForm['monto'];
        this.numeroCuenta = jsnForm['numeroCuenta'];
        this.terminal = jsnForm['terminal'] || "TERM5001";
        this.tipo = jsnForm['tipo'];
        this.usuario = jsnForm['usuario'] || "u-5001";
    }
}
