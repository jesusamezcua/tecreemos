import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Usuario } from 'src/app/modelos/Usuario/usuario';
import { UsuarioService } from 'src/app/servicios/auth/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm = new FormGroup({
    email: new FormControl('desarrollo@prueba.com', [Validators.required]),
    password: new FormControl('pruebas001', [ Validators.required]),
    returnSecureToken: new FormControl(true)
  });

  constructor(private userService: UsuarioService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  public async fnLogin() {
    try {
      const user = new Usuario(this.loginForm.value);
      let res: any = await this.userService.fnsLogin(user);
      if(res['idToken']){
        this.userService.setToken(res['idToken']);//Guardo el idToken en el localstorage
        this.router.navigateByUrl('/');
      }else{
        this.toastr.error("Ocurrio un error !!!");
      }
    } catch (error) {
      this.toastr.error("Error al iniciar sesión!!!");
    }
  }

}
