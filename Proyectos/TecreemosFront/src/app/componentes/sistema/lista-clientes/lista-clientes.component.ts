import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/modelos/Cliente/cliente';
import { LocalDataSource } from 'ng2-smart-table';
import Swal from 'sweetalert2';
import { ClienteService } from 'src/app/servicios/cliente/cliente.service';
import { ButtomComponent } from '../botonGeneral.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.scss']
})
export class ListaClientesComponent implements OnInit {

  private listaClientes: Cliente[] = [];
  public digital_list: LocalDataSource = new LocalDataSource();
  public boolDatos: Boolean = false;

  public settings = {
    actions: false,
    columns: {
      //id: { title: 'Id Cliente' },
      strRFC: { title: 'RFC' },
      strNombre: { title: 'Nombre' },
      strApellidoPaterno: { title: 'Apellido Paterno' },
      strApellidoMaterno: { title: 'Apellido Materno' },
      strDireccion: { title: 'Dirección' },
      strGenero: { title: 'Genero' },
      intEdad: { title: 'Edad' },
      actions:{
        title:'Acciones',
        type:'custom',
        renderComponent: ButtomComponent,
        onComponentInitFunction: (instance: any) => {
          instance.onSeleccionar.subscribe((id: any) => {
            this.router.navigateByUrl(`/listaCuentas/${id}`);
          });
          instance.onEliminar.subscribe((id: any) => {
            Swal.fire({
              title: 'Eliminar Cliente',
              text: 'No se podra revertir la acción!',
              icon: 'warning',
              confirmButtonText: 'Eliminar',
              denyButtonText: 'Cerrar',
              showDenyButton: true,
            }).then(result => {
              let { isConfirmed } = result;
              if(isConfirmed){
                this.fnEliminarCliente(id);
              }
            })
          });
          instance.onEditar.subscribe((id: any) => {
            this.router.navigateByUrl(`/agregarCliente/${id}`);
          })
        }
      }
    },
  };

  constructor(private clienteService: ClienteService, private router: Router, private toastr: ToastrService) { }

  async ngOnInit(): Promise<void> {
    await this.fnCargarClientes();
  }

  private async fnCargarClientes() {
    try {
      this.listaClientes = await this.clienteService.fnsGetClientes();
      if (this.listaClientes.length > 0) {
        this.digital_list = new LocalDataSource(this.listaClientes);
        this.boolDatos = true;
      }
      else {
        this.boolDatos = false;
      }
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }

  public CerrarSesion(){
    delete localStorage.idToken;
    this.router.navigateByUrl('/login');
  }

  private async fnEliminarCliente(idCliente: string) {
    try {
      await this.clienteService.fnsEliminarCliente(idCliente);
      this.toastr.success('Cliente eliminado', '');
      this.fnCargarClientes();
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }
}