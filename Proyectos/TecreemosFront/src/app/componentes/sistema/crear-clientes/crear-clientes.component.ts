import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Resp } from 'src/app/modelos/General/resp';
import { ClienteService } from 'src/app/servicios/cliente/cliente.service';
import { Cliente } from 'src/app/modelos/Cliente/cliente';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-crear-clientes',
  templateUrl: './crear-clientes.component.html',
  styleUrls: ['./crear-clientes.component.scss']
})
export class CrearClientesComponent implements OnInit {

  private res: Resp = new Resp;
  private suscribe: Subscription;
  public idCliente: string = '';
  public boolEditar: boolean = false;

  public clienteForm = new FormGroup({
    strNombre: new FormControl('', [Validators.required]),
    strApellidoPaterno: new FormControl('', [ Validators.required]),
    strApellidoMaterno: new FormControl('', [ Validators.required]),
    strRFC: new FormControl('', [Validators.required, Validators.minLength(13)]),
    strDireccion: new FormControl('', [Validators.required]),
    strGenero: new FormControl('', [ Validators.required]),
    intEdad: new FormControl(0,[Validators.required, Validators.min(18), Validators.max(100)])
  });

  constructor(private clienteService: ClienteService, private routerService: Router, private routeService: ActivatedRoute, private toastr: ToastrService) {
    this.suscribe = new Subscription();
  }

  async ngOnInit(): Promise<void> {
    this.suscribe.add(
      this.routeService.paramMap.subscribe(params => {
        let idCliente = params.get('idCliente');
        if (idCliente) {
          this.boolEditar = true;
          this.idCliente = idCliente;
          this.fnCargar();
        }
      })
    )
  }

  private async fnCargar(){
    try {
      let cliente: Cliente = new Cliente(await this.clienteService.fnsGetCliente(this.idCliente));
      this.clienteForm.patchValue(cliente);
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }

  public async fnGuardar(){
    try {
      let cliente: Cliente = new Cliente(this.clienteForm.value);
      cliente.strRFC = cliente.strRFC.toLocaleUpperCase();
      this.res = await this.clienteService.fnsAgregarCliente(cliente);
      if(this.res.success){
        this.toastr.success("Cliente creado");
        this.routerService.navigateByUrl("");
      }else{
        this.toastr.error(this.res.message);
      }
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }

  public async fnEditar(){
    try {
      let cliente: Cliente = new Cliente(this.clienteForm.value);
      cliente.id = this.idCliente;
      await this.clienteService.fnsModificarCliente(cliente, this.idCliente);
      this.toastr.success("Cliente modificado");
      this.routerService.navigateByUrl("");
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }
}