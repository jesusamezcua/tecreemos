import {Component, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'custom-component',
    template: `
    <button title="Seleccionar" class="btn" (click)="fnSeleccionar(rowData)">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right-circle" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/>
        </svg>
    </button>
    <button title="Depositar" class="btn" (click)="fnDepositar(rowData.numeroCuenta)">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-plus" viewBox="0 0 16 16">
            <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
            <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
        </svg>
    </button>
    <button title="Retirar" class="btn" (click)="fnRetirar(rowData.numeroCuenta)">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-minus" viewBox="0 0 16 16">
            <path d="M5.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
            <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
        </svg>
    </button>`
})
export class ButtomCuentaComponent {

    rowData: any;

   @Output() onRetirar: EventEmitter<any> = new EventEmitter();
   @Output() onSeleccionar: EventEmitter<any> = new EventEmitter();
   @Output() onDepositar: EventEmitter<any> = new EventEmitter();

   fnRetirar(numeroCuenta: any){
        this.onRetirar.emit(numeroCuenta);
    }

    fnDepositar(numeroCuenta: any){
        this.onDepositar.emit(numeroCuenta);
    }

    fnSeleccionar(jsonCuenta: any){
        this.onSeleccionar.emit(jsonCuenta);
    }
}