import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CrearClientesComponent } from './crear-clientes/crear-clientes.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListaCuentasComponent } from './lista-cuentas/lista-cuentas.component';
import { CrearCuentasComponent } from './crear-cuentas/crear-cuentas.component';
import { VerTransaccionesComponent } from './ver-transacciones/ver-transacciones.component';
import { CrearTransaccionesComponent } from './crear-transacciones/crear-transacciones.component';

const routes: Routes = [
  { path: 'agregarCliente', component: CrearClientesComponent },
  { path: 'agregarCliente/:idCliente', component: CrearClientesComponent },
  { path: '', component: ListaClientesComponent },
  { path: 'listaCuentas/:idCliente', component: ListaCuentasComponent },
]

@NgModule({
  declarations: [ListaClientesComponent, CrearClientesComponent, ListaCuentasComponent, CrearCuentasComponent, VerTransaccionesComponent, CrearTransaccionesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})
export class SistemaModule { }
