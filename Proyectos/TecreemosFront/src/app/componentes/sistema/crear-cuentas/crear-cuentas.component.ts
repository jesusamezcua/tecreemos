import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Cliente } from 'src/app/modelos/Cliente/cliente';
import { Cuenta } from 'src/app/modelos/Cuenta/cuenta';
import { ClienteService } from 'src/app/servicios/cliente/cliente.service';
import { BsModalRef } from 'ngx-bootstrap/modal/';
import { CuentasService } from 'src/app/servicios/cuentas/cuentas.service';

@Component({
  selector: 'app-crear-cuentas',
  templateUrl: './crear-cuentas.component.html',
  styleUrls: ['./crear-cuentas.component.scss']
})
export class CrearCuentasComponent implements OnInit {

  @Input() jsonCliente: Cliente = new Cliente({});
  @Input() jsonCuentas: Cuenta[] = [];
  @Input() modalRef: BsModalRef | undefined;
  @Output() nuevaCuenta = new EventEmitter();

  public cuentaForm = new FormGroup({
    numeroCuenta: new FormControl(0,[Validators.required, Validators.min(0)]),
    saldo: new FormControl(0,[Validators.required, Validators.min(0)]),
    idCliente: new FormControl('')
  });

  constructor(private cuentaService: CuentasService, private toastr: ToastrService, private router: Router) {}

  ngOnInit(): void {
  }

  public async fnGuardar(){
    try {
      let cuenta: Cuenta = new Cuenta(this.cuentaForm.value);
      if(await this.fnValidarCuenta(cuenta)){
        cuenta.idCliente = this.jsonCliente.strRFC;
        await this.cuentaService.fnsCrearCuenta(cuenta);
        this.toastr.success("Cuenta creada");
        this.nuevaCuenta.emit();
        this.modalRef?.hide();
      }else{
        this.toastr.error("Ese Numero de cuenta ya existe para este Cliente");
      }
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }

  private async fnValidarCuenta(cuenta: Cuenta): Promise<boolean>{
    let cuentaDuplicada = this.jsonCuentas.filter((x) => x.numeroCuenta == cuenta.numeroCuenta);
    if(cuentaDuplicada.length > 0){
      return false;
    }else{
      return true;
    }
  }
}