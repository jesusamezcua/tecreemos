import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Cliente } from 'src/app/modelos/Cliente/cliente';
import { Cuenta } from 'src/app/modelos/Cuenta/cuenta';
import { ClienteService } from 'src/app/servicios/cliente/cliente.service';
import { CuentasService } from 'src/app/servicios/cuentas/cuentas.service';
import { ButtomCuentaComponent } from '../botonCuentas.component';

@Component({
  selector: 'app-lista-cuentas',
  templateUrl: './lista-cuentas.component.html',
  styleUrls: ['./lista-cuentas.component.scss']
})
export class ListaCuentasComponent implements OnInit {

  public listaCuentas: Cuenta[] = [];
  public digital_list: LocalDataSource = new LocalDataSource();
  public boolDatos: Boolean = false;
  private suscribe: Subscription;
  private idCliente: string = '';
  public isDeposito: boolean = false;
  public jsonCliente: Cliente = new Cliente({});
  public modalRef: BsModalRef | undefined;
  public jsonCuenta: Cuenta = new Cuenta({});
  public numeroCuenta: number = 0;
  public strModalOption: 'crearCuenta' | 'verTransacciones' | 'crearTransacciones' = 'crearCuenta';
  @ViewChild('modalTemplate') modalTemplate!: TemplateRef<any>;

  public settings = {
    actions: false,
    columns: {
      numeroCuenta: { title: 'Numero de cuenta' },
      estado: { title: 'Estado' },
      fechaUltimaAct: { title: 'Fecha ultima actualización' },
      idCliente: { title: 'RFC Cliente' },
      //saldo: { title: 'Saldo' },
      actions:{
        title:'Acciones',
        type:'custom',
        renderComponent: ButtomCuentaComponent,
        onComponentInitFunction: (instance: any) => {
          instance.onSeleccionar.subscribe((jsonCuenta: any) => {
            this.fnVerTransacciones(jsonCuenta);
          });
          instance.onRetirar.subscribe((numeroCuenta: any) => {
            this.fnCrearTransacciones(numeroCuenta, false)
          });
          instance.onDepositar.subscribe((numeroCuenta: any) => {
            this.fnCrearTransacciones(numeroCuenta, true)
          })
        }
      }
    },
  };

  constructor(private routeService: ActivatedRoute, private clienteService: ClienteService, private toastr: ToastrService, private cuentaService: CuentasService, private modalService: BsModalService) { 
    this.suscribe = new Subscription();
  }

  ngOnInit(): void {
    this.suscribe.add(
      this.routeService.paramMap.subscribe(params => {
        let idCliente = params.get('idCliente');
        if (idCliente) {
          this.idCliente = idCliente;
          this.fnCargarCliente();
          this.fnObtenerCuentas();
        }
      })
    )
  }

  public fnOpenCrearCuenta() {
    this.strModalOption = 'crearCuenta';
    this.modalRef = this.modalService.show(this.modalTemplate);
  }

  private fnVerTransacciones(jsonCuenta: Cuenta) {
    this.jsonCuenta = jsonCuenta;
    this.strModalOption = 'verTransacciones';
    this.modalRef = this.modalService.show(this.modalTemplate);
  }

  private fnCrearTransacciones(numeroCuenta: number, isDeposito: boolean) {
    this.numeroCuenta = numeroCuenta;
    this.isDeposito = isDeposito;
    this.strModalOption = 'crearTransacciones';
    this.modalRef = this.modalService.show(this.modalTemplate);
  }

  private async fnCargarCliente(){
    try {
      this.jsonCliente = new Cliente(await this.clienteService.fnsGetCliente(this.idCliente));
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }

  public async fnObtenerCuentas(){
    try {
      let res = await this.cuentaService.fnsObtenerCuentas();
      let cuentas = Object.values(res);
      this.listaCuentas = cuentas.filter(item => item.idCliente == this.jsonCliente.strRFC);
      if (this.listaCuentas.length > 0) {
        this.digital_list = new LocalDataSource(this.listaCuentas);
        this.boolDatos = true;
      }
      else {
        this.boolDatos = false;
      }
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }
}