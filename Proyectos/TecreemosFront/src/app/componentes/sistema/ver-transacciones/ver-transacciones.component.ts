import { Component, Input, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Cuenta } from 'src/app/modelos/Cuenta/cuenta';
import { CuentasService } from 'src/app/servicios/cuentas/cuentas.service';

@Component({
  selector: 'app-ver-transacciones',
  templateUrl: './ver-transacciones.component.html',
  styleUrls: ['./ver-transacciones.component.scss']
})
export class VerTransaccionesComponent implements OnInit {

  @Input() modalRef: BsModalRef | undefined;
  @Input() jsonCuenta: Cuenta =  new Cuenta({});
  private listaTransacciones: any;
  public boolDatos: Boolean = false;
  public digital_list: LocalDataSource = new LocalDataSource();

  public settings = {
    actions: false,
    columns: {
      fechaUltimaAct: { title: 'Fecha' },
      terminal: { title: 'Terminal' },
      usuario: { title: 'Usuario' },
      tipo: { title: 'Tipo' },
      monto: { title: 'Monto' },
      saldoAnterior: { title: 'Saldo anterior' },
      saldo: { title: 'Saldo actual' },
    },
  };

  constructor(private cuentaService: CuentasService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.fnObtenerTransacciones()
  }

  private async fnObtenerTransacciones(){
    try {
      let res = await this.cuentaService.fnsObtenerTransacciones();
      let transacciones = Object.values(res);
      this.listaTransacciones = transacciones.filter(item => item.numeroCuenta == this.jsonCuenta.numeroCuenta);
      let saldoTotal = this.jsonCuenta.saldo;
      this.listaTransacciones.forEach((element: any) => {
        element.saldoAnterior = saldoTotal;
        if(element.tipo == 'Deposito'){
          saldoTotal = saldoTotal + element.monto;
        }else{
          saldoTotal = saldoTotal - element.monto;
        }
        element.saldo = saldoTotal;
      });
      if (this.listaTransacciones.length > 0) {
        this.digital_list = new LocalDataSource(this.listaTransacciones);
        this.boolDatos = true;
      }
      else {
        this.boolDatos = false;
      }
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }
}