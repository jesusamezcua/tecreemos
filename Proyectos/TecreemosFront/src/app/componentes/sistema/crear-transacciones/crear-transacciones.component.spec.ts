import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearTransaccionesComponent } from './crear-transacciones.component';

describe('CrearTransaccionesComponent', () => {
  let component: CrearTransaccionesComponent;
  let fixture: ComponentFixture<CrearTransaccionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearTransaccionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearTransaccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
