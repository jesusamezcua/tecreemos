import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Transaccion } from 'src/app/modelos/Transaccion/transaccion';
import { CuentasService } from 'src/app/servicios/cuentas/cuentas.service';

@Component({
  selector: 'app-crear-transacciones',
  templateUrl: './crear-transacciones.component.html',
  styleUrls: ['./crear-transacciones.component.scss']
})
export class CrearTransaccionesComponent implements OnInit {

  @Input() modalRef: BsModalRef | undefined;
  @Input() numeroCuenta: number = 0;
  @Input() isDeposito: boolean = false;
  @Output() nuevaTransaccion = new EventEmitter();

  public transaccionForm = new FormGroup({
    monto: new FormControl(0,[Validators.required, Validators.min(0)])
  });

  constructor(private toastr: ToastrService, private cuentaService: CuentasService) { }

  ngOnInit(): void {
  }

  public async fnGuardar(){
    try {
      let transaccion: Transaccion = new Transaccion(this.transaccionForm.value);
      transaccion.numeroCuenta = this.numeroCuenta;
      transaccion.tipo = this.isDeposito ? 'Deposito': 'Retiro';
      await this.cuentaService.fnsCrearTransaccion(transaccion);
      this.toastr.success("Transaccion Realizada");
      this.nuevaTransaccion.emit();
      this.modalRef?.hide();
    } catch (error) {
      this.toastr.error("Error en el servidor !!!");
    }
  }
}