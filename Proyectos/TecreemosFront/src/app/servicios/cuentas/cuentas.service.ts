import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cuenta } from 'src/app/modelos/Cuenta/cuenta';
import { Transaccion } from 'src/app/modelos/Transaccion/transaccion';
import { environment } from 'src/environments/environment';
import { UsuarioService } from '../auth/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class CuentasService {

  readonly strUrl = environment.strUrlCuentas+this.userService.getToken();
  readonly strUrlTransacciones = environment.strUrlTransacciones+this.userService.getToken();

  constructor(private http: HttpClient, private userService: UsuarioService) {
  }

  public fnsCrearCuenta(cuenta: Cuenta) {
    return this.http.post(this.strUrl, cuenta).toPromise();
  }

  public fnsObtenerCuentas() {
    return this.http.get(this.strUrl).toPromise();
  }

  public fnsObtenerTransacciones() {
    return this.http.get(this.strUrlTransacciones).toPromise();
  }

  public fnsCrearTransaccion(transaccion: Transaccion) {
    return this.http.post(this.strUrlTransacciones, transaccion).toPromise();
  }
}
