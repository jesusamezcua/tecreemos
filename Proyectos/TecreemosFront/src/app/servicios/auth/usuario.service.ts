import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Usuario } from 'src/app/modelos/Usuario/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  readonly strUrl = environment.strUrlLogin;

  constructor(private http: HttpClient) { }

  public fnsLogin(user: Usuario) {
    return this.http.post(this.strUrl, user).toPromise();
  }

  public setToken(idToken: string) {
    localStorage.setItem("idToken", idToken);
  }

  public getToken() {
    return localStorage.getItem("idToken");
  }
}
