import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cliente } from 'src/app/modelos/Cliente/cliente';
import { Resp } from 'src/app/modelos/General/resp';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  readonly strUrl = environment.strUrl;

  constructor(private http: HttpClient) {
  }

  public fnsGetClientes(): Promise<Cliente[]> {
    return this.http.get<Cliente[]>(this.strUrl+'/clientes').toPromise();
  }

  public fnsEliminarCliente(id: string){
    return this.http.delete(this.strUrl+'/clientes/'+id).toPromise();
  }

  public fnsAgregarCliente(cliente: Cliente): Promise<Resp> {
    return this.http.post<Resp>(this.strUrl+'/clientes', cliente).toPromise();
  }

  public fnsGetCliente(id: string): Promise<Cliente> {
    return this.http.get<Cliente>(this.strUrl+'/clientes/'+id).toPromise();
  }

  public fnsModificarCliente(cliente: Cliente, id: string) {
    return this.http.put(this.strUrl+'/clientes/'+id, cliente).toPromise();
  }
}
